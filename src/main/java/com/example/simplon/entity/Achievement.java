package com.example.simplon.entity;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

public class Achievement {

	@Min(1)
	private Integer id;
	@NotBlank(message = "icon is mandatory")
	private String icon;
	@NotBlank(message = "Label is mandatory")
	private String label;

	public Achievement() {
	}

	public Achievement(String icon, String label) {
		this.icon = icon;
		this.label = label;
	}

	public Achievement(Integer id, String icon, String label) {
		this.id = id;
		this.icon = icon;
		this.label = label;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
}
