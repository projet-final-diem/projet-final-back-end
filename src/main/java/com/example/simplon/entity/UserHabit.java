package com.example.simplon.entity;

import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import jdk.jfr.Label;

public class UserHabit {
	@Min(1)
	private Integer id;
	@Min(1)
	private Integer userId;
	private Integer habitId;
	@NotBlank(message = "Label is mandatory")
	private String label;
	@NotBlank(message = "Icon is mandatory")
	private String icon;
	private HabitStatus dailyHabitStatus;
	private Habit habit = null;

	public UserHabit() {
	}

	public UserHabit(Integer userId, Integer habitId, String label, String icon) {
		this.userId = userId;
		this.habitId = habitId;
		this.label = label;
		this.icon = icon;
	}

	public UserHabit(Integer id, Integer userId, Integer habitId, String label, String icon) {
		this.id = id;
		this.userId = userId;
		this.habitId = habitId;
		this.label = label;
		this.icon = icon;
	}

	public UserHabit(Integer id, Integer userId, Integer habitId, String label, String icon,
			HabitStatus dailyHabitStatus) {
		this.id = id;
		this.userId = userId;
		this.habitId = habitId;
		this.label = label;
		this.icon = icon;
		this.dailyHabitStatus = dailyHabitStatus;
	}

	public UserHabit(String label, String icon) {
		this.label = label;
		this.icon = icon;
	}

	public UserHabit(Integer userId, String label, String icon) {
		this.userId = userId;
		this.label = label;
		this.icon = icon;
	}

	public UserHabit(Integer userId, String label, String icon, Habit habit) {
		this.userId = userId;
		this.label = label;
		this.icon = icon;
		this.habit = habit;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Habit getHabit() {
		return habit;
	}

	public void setHabit(Habit habit) {
		this.habit = habit;
	}

	public Integer getHabitId() {
		return habitId;
	}

	public void setHabitId(Integer habitId) {
		this.habitId = habitId;
	}

	public HabitStatus getDailyHabitStatus() {
		return dailyHabitStatus;
	}

	public void setDailyHabitStatus(HabitStatus dailyHabitStatus) {
		this.dailyHabitStatus = dailyHabitStatus;
	}
}
