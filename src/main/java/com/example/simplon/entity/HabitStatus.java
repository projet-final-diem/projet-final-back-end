package com.example.simplon.entity;

import java.time.LocalDate;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

public class HabitStatus {
	@Min(1)
	private Integer id;
	@NotBlank(message = "forDate is mandatory")
	private LocalDate forDate;
	private String status;
	private Integer UserHabitID;

	public HabitStatus(LocalDate forDate, String status, Integer userHabitID) {
		this.forDate = forDate;
		this.status = status;
		UserHabitID = userHabitID;
	}

	public HabitStatus(Integer id, LocalDate forDate, String status, Integer userHabitID) {
		this.id = id;
		this.forDate = forDate;
		this.status = status;
		UserHabitID = userHabitID;
	}

	public HabitStatus() {
	}

	public HabitStatus(Integer id, Integer userIdStatus, LocalDate forDate, String status) {
		this.id = id;
		this.UserHabitID = userIdStatus;
		this.forDate = forDate;
		this.status = status;
	}

	public HabitStatus(LocalDate forDate, String status) {
		this.forDate = forDate;
		this.status = status;
	}

	public HabitStatus(Integer id, LocalDate forDate, String status) {
		this.id = id;
		this.forDate = forDate;
		this.status = status;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LocalDate getForDate() {
		return forDate;
	}

	public void setForDate(LocalDate forDate) {
		this.forDate = forDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getUserHabitID() {
		return UserHabitID;
	}

	public void setUserHabitID(Integer userHabitID) {
		UserHabitID = userHabitID;
	}

}
