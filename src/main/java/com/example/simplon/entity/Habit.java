package com.example.simplon.entity;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

public class Habit {

	@Min(1)
	private Integer id;
	@NotBlank(message = "Label is mandatory")
	private String label;
	@NotBlank(message = "Label is mandatory")
	private String icon;

	public Habit() {
	}

	public Habit(String label, String icon) {
		this.label = label;
		this.icon = icon;
	}

	public Habit(Integer id, String label, String icon) {
		this.id = id;
		this.label = label;
		this.icon = icon;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
}
