package com.example.simplon.repository;

import static java.sql.Types.NULL;

import com.example.simplon.entity.HabitStatus;
import com.example.simplon.entity.User;
import com.example.simplon.entity.UserHabit;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserHabitRepository {

	@Autowired
	DataSource dataSource;

	public boolean save(UserHabit userHabit) {
		try(Connection connection = dataSource.getConnection()) {
			PreparedStatement stmt = connection.prepareStatement(
					"INSERT INTO user_habits(user_id, habits_id, label, icon)"
							+ " VALUES (?, ?, ?, ?)",
					PreparedStatement.RETURN_GENERATED_KEYS
			);
			stmt.setInt(1, userHabit.getUserId());
			stmt.setInt(2, userHabit.getHabitId());
			stmt.setString(3, userHabit.getLabel());
			stmt.setString(4, userHabit.getIcon());

			boolean done = stmt.executeUpdate() == 1;
			ResultSet rs = stmt.getGeneratedKeys();
			if(rs.next()) {
				userHabit.setId((rs.getInt(1)));
			}
			return done;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public List<UserHabit> getDailyHabitsFeedByUserId(int id) {
		List<UserHabit> list = new ArrayList<>();
		try(Connection connection = dataSource.getConnection()) {
			PreparedStatement stmt = connection.prepareStatement(
					"SELECT * FROM user_habits AS uh"
							+ " LEFT JOIN habits_status AS hs"
							+ " ON uh.id = user_habits_id"
							+ " AND for_date = CURRENT_DATE"
							+ " WHERE uh.user_id = ?"
			);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				UserHabit userHabit = new UserHabit(
						rs.getInt("uh.id"),
						rs.getInt("user_id"),
						rs.getInt("habits_id"),
						rs.getString("label"),
						rs.getString("icon")
				);
				HabitStatus habitStatus = new HabitStatus(
						rs.getInt("hs.id"),
						LocalDate.now(),
						rs.getString("status"),
						rs.getInt("uh.id")
				);
					userHabit.setDailyHabitStatus(habitStatus);
				list.add(userHabit);
			}
			return list;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public List<UserHabit> getAllUserHabitByUserId(int id) {
		List<UserHabit> userHabitsList = new ArrayList<>();
		try(Connection connection = dataSource.getConnection()) {
			PreparedStatement stmt = connection.prepareStatement(
					"SELECT * FROM user_habits WHERE user_id = ?"
			);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				UserHabit userHabit = new UserHabit(
						rs.getInt("id"),
						rs.getInt("user_id"),
						rs.getInt("habits_id"),
						rs.getString("label"),
						rs.getString("icon")
				);
				userHabitsList.add(userHabit);
			}
			return userHabitsList;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public UserHabit getById(int id) {
		return null;
	}

	public boolean delete(int id) {
		try(Connection connection = dataSource.getConnection()) {
			PreparedStatement stmt = connection.prepareStatement(
					"DELETE FROM user_habits WHERE id = ?"
			);
			stmt.setInt(1, id);
			return (stmt.executeUpdate() == 1);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}






}
