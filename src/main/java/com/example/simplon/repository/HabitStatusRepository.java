package com.example.simplon.repository;

import com.example.simplon.entity.HabitStatus;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class HabitStatusRepository {

	@Autowired
	DataSource dataSource;

	public boolean save(HabitStatus habitStatus) {
		try(Connection connection = dataSource.getConnection()) {
			PreparedStatement stmt = connection.prepareStatement(
					"INSERT INTO habits_status(user_habits_id, for_date, status)"
							+ " VALUES (?, ?, ?)",
					PreparedStatement.RETURN_GENERATED_KEYS
			);
			stmt.setInt(1, habitStatus.getUserHabitID());
			stmt.setDate(2, Date.valueOf(habitStatus.getForDate()));
			stmt.setString(3, habitStatus.getStatus());
			boolean done = stmt.executeUpdate() == 1;
			ResultSet rs = stmt.getGeneratedKeys();
			if(rs.next()) {
				habitStatus.setId((rs.getInt(1)));
			}
			return done;

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public boolean updateStatusById(HabitStatus habitStatus) {
		try(Connection connection = dataSource.getConnection()) {
			PreparedStatement stmt = connection.prepareStatement(
					"UPDATE habits_status SET status = ? WHERE id = ? "
			);
			stmt.setString(1, habitStatus.getStatus());
			stmt.setInt(2, habitStatus.getId());
			return (stmt.executeUpdate() == 1);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}


}
