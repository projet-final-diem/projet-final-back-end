package com.example.simplon.repository;

import com.example.simplon.entity.Habit;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class HabitRepository {

	@Autowired
	DataSource dataSource;

	public List<Habit> findAll() {
		List<Habit> list = new ArrayList<>();
		try (Connection connection = dataSource.getConnection()) {
			PreparedStatement stmt = connection.prepareStatement(
					"SELECT * FROM habits"
			);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Habit habit = new Habit(
						rs.getInt("id"),
						rs.getString("label"),
						rs.getString("icon")
				);
				list.add(habit);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

		return list;
	}

	public Habit findById(int id) {
		try(Connection connection = dataSource.getConnection()) {
			PreparedStatement stmt = connection.prepareStatement(
					"SELECT * FROM habits WHERE id = ?"
			);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				Habit habit = new Habit(
						rs.getInt("id"),
						rs.getString("label"),
						rs.getString("icon")
				);
				return habit;
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return null;
	}

	public List<Habit> getHabitsMinusAlreadySubscribedByUser(int id) {
		List<Habit> list = new ArrayList<>();
		try (Connection connection = dataSource.getConnection()) {
			PreparedStatement stmt = connection.prepareStatement(
					"SELECT * FROM habits "
							+ "WHERE id NOT IN"
							+ "("
							+ "SELECT habits_id "
							+ "FROM user_habits "
							+ "WHERE user_id = ? "
							+ "AND habits_id IS NOT NULL"
							+ ");"
			);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()) {
				Habit newHabit = new Habit(
						rs.getInt("id"),
						rs.getString("label"),
						rs.getString("icon")
				);
				list.add(newHabit);
			}
			return list;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}





	//SELECT * FROM user_habits AS uh
	//LEFT JOIN habits_status AS hs
	//ON uh.id = user_habits_id
	//WHERE uh.user_id = 1
	//AND for_date = '2022-09-12';


}
