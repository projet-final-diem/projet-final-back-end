package com.example.simplon.repository;

import com.example.simplon.entity.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepository {

	@Autowired
	DataSource dataSource;

	//CREATE
	public boolean save(User user) {
		try(Connection connection = dataSource.getConnection() ) {
			PreparedStatement stmt = connection.prepareStatement(
					"INSERT INTO user(username, email, password) "
							+ "VALUES (?, ?, ?)", PreparedStatement.RETURN_GENERATED_KEYS
			);
			stmt.setString(1, user.getUserName());
			stmt.setString(2, user.getEmail());
			stmt.setString(3, user.getPassword());
			boolean done = stmt.executeUpdate() == 1;
			ResultSet rs = stmt.getGeneratedKeys();
			if(rs.next()) {
				user.setId((rs.getInt(1)));
			}
			return done;
		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException("Database error");
		}
	}

	//READ

	public User findUserById(int id) {
		try(Connection connection = dataSource.getConnection()) {
			PreparedStatement stmt = connection.prepareStatement(
					"SELECT * FROM user WHERE id = ?"
			);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			if(rs.next()) {
				User user = new User(
						rs.getInt("id"),
						rs.getString("username"),
						rs.getString("email"),
						rs.getString("password")
				);
				return user;
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return null;
	}

	public User findUserByEmail(String email) {
		try(Connection connection = dataSource.getConnection()) {
			PreparedStatement stmt = connection.prepareStatement(
					"SELECT * FROM user WHERE email = ?"
			);
			stmt.setString(1, email);
			ResultSet rs = stmt.executeQuery();
			if(rs.next()) {
				User user = new User(
						rs.getInt("id"),
						rs.getString("username"),
						rs.getString("email"),
						rs.getString("password")
				);
				return user;
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return null;
	}



}
