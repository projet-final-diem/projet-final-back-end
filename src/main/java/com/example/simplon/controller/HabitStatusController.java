package com.example.simplon.controller;

import com.example.simplon.entity.HabitStatus;
import com.example.simplon.repository.HabitRepository;
import com.example.simplon.repository.HabitStatusRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/habit-status")
public class HabitStatusController {

	private static final Logger logger = LoggerFactory.getLogger(HabitStatusController.class);

	@Autowired
	HabitStatusRepository habitStatusRepository;

	@PostMapping
	public HabitStatus save(@RequestBody HabitStatus habitStatus) {
		habitStatusRepository.save(habitStatus);
		return habitStatus;
	}

	@PutMapping
	public void updateHabitStatus(@RequestBody HabitStatus habitStatus) {
		boolean updated = habitStatusRepository.updateStatusById(habitStatus);
		if(updated != true) {
			logger.warn("try to update habitStatus with id :" + habitStatus.getId() + "but not found");
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		logger.info(
				"userHabit with id : " + habitStatus.getId() +
				" and userHabitId : " + habitStatus.getUserHabitID() +
				" changed to " + habitStatus.getStatus()
		);
	}
}

