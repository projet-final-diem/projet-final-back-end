package com.example.simplon.controller;

import com.example.simplon.entity.User;
import com.example.simplon.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	UserRepository userRepository;

	@Autowired
	private PasswordEncoder encoder;

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@PostMapping
	public User save(@RequestBody User user) {
		if (userRepository.findUserByEmail(user.getEmail()) != null) {
			logger.warn("Try to create an account with mail " + user.getEmail() + "that already exists");
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "User already exists");
		}
		user.setId(null);
		String hashed = encoder.encode(user.getPassword());
		user.setPassword(hashed);
		userRepository.save(user);
		SecurityContextHolder.getContext().setAuthentication(
				new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities()));
		logger.info("Created user : " +
				user.getUserName() +
				" - " + user.getEmail() +
				" - " + user.getPassword()
		);
		return user;
	}

	@GetMapping("/id/login")
	public User login(@AuthenticationPrincipal User user) {
		logger.info("User : " + user.getUserName() + " with Email : " + user.getEmail() + " logged");
		return user;
	}

	@GetMapping("/verify/{email}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void userExists(@PathVariable String email) {
		if (userRepository.findUserByEmail(email) == null) {
			logger.warn("Email : " + email + " checked and not found");
			throw new ResponseStatusException((HttpStatus.NOT_FOUND));
		}
		logger.info("Email : " + email + " checked and exists");
	}

	@GetMapping("/{id}")
	public User getById(@PathVariable int id) {
		User user = userRepository.findUserById(id);
		if(user == null) {
			logger.warn("Try to find user with id : " + id + "but doesn't exist");
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		logger.info("Get the user with id : " + id);
		return user;
	}
}
