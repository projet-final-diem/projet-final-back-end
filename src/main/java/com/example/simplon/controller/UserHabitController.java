package com.example.simplon.controller;


import com.example.simplon.entity.UserHabit;
import com.example.simplon.repository.UserHabitRepository;
import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/userhabit")
public class UserHabitController {

	private static final Logger logger = LoggerFactory.getLogger(UserHabitController.class);

	@Autowired
	UserHabitRepository userHabitRepository;

	@PostMapping
	public UserHabit save(@RequestBody UserHabit userHabit) {
		userHabitRepository.save(userHabit);
		return userHabit;
	}

	@GetMapping("/{userId}")
	public List<UserHabit> getHabitByUserId(@PathVariable int userId) {
		return userHabitRepository.getAllUserHabitByUserId(userId);
	}

	@GetMapping("/today/{userId}")
	public List<UserHabit> getDailyHabitsFeedByUserId(@PathVariable int userId) {
		return userHabitRepository.getDailyHabitsFeedByUserId(userId);
	}

	@GetMapping("/unique/{id}")
	public UserHabit getById() {
		return null;
	}

	@DeleteMapping("/{userHabitId}")
	public void delete(@PathVariable int userHabitId) {
		boolean done = userHabitRepository.delete(userHabitId);
		if(!done) {
			logger.debug("Something goes wrong when try to delete userHabit with id : " + userHabitId);
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		logger.info("UserHabit with id : " + userHabitId + "has been deleted");
	}
}
