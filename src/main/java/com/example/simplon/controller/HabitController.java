package com.example.simplon.controller;

import com.example.simplon.entity.Habit;
import com.example.simplon.entity.User;
import com.example.simplon.repository.HabitRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/habit")
public class HabitController {

	@Autowired
	HabitRepository habitRepository;

	@GetMapping("/user/{id}")
	public List<Habit> getAllWithoutUserAlreadySubscribe(@PathVariable int id) {
		return habitRepository.getHabitsMinusAlreadySubscribedByUser(id);
	}

	@GetMapping("/{id}")
	public Habit getById(@PathVariable int id) {
		Habit habit = habitRepository.findById(id);
		if(habit == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
		return habit;
	}
}
