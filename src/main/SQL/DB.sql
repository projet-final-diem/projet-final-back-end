DROP DATABASE `projet_final`;

CREATE DATABASE IF NOT EXISTS `projet_final`;

USE `projet_final`;

CREATE TABLE IF NOT EXISTS `user`(
    `id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `username` VARCHAR(50),
    `email` VARCHAR(250),
    `password` VARCHAR(1000)
    );

CREATE TABLE IF NOT EXISTS achievement(
    `id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `label` VARCHAR(500),
    `icon` VARCHAR(500)
);

CREATE TABLE IF NOT EXISTS user_achievement(
    `user_id` INT,
    CONSTRAINT FK_user
    FOREIGN KEY (user_id)
    REFERENCES `user`(id)
    ON DELETE CASCADE,
    `achievement_id` INT,
    CONSTRAINT FK_achievement
    FOREIGN KEY (achievement_id)
    REFERENCES achievement(id)
    ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS habits(
    `id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `label` VARCHAR(100),
    `icon` VARCHAR(500)
);

CREATE TABLE IF NOT EXISTS user_habits(
    `id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `user_id` INT,
    `habits_id` INT,
    `label` VARCHAR(100),
    `icon` VARCHAR(500),
    CONSTRAINT FK_user_habits
    FOREIGN KEY (user_id)
    REFERENCES `user`(id)
    ON DELETE CASCADE,
    CONSTRAINT FK_habits_user_habits
    FOREIGN KEY (habits_id)
    REFERENCES habits(id)
    ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS habits_status(
    `id` INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
    `user_habits_id` INT,
    `for_date` DATE,
    `status` VARCHAR(50),
    CONSTRAINT FK_user_habits_habits_status
    FOREIGN KEY (user_habits_id)
    REFERENCES user_habits(id)
    ON DELETE CASCADE
);

INSERT INTO user(username, email, password)
VALUES ('Elodie', 'elodie@test.com', '1234'),
       ('Louis', 'louis@test.com', '1233'),
       ('Nicolas', 'nicolas@test.com', '1234');

INSERT INTO achievement(label, icon)
VALUES ('7 jours d\'affilé!', 'static/achievement.png'),
       ('14 jours d\'affilé!', 'static/achievement.png'),
       ('Même le mercredi', 'static/achievement.png');

INSERT INTO user_achievement(user_id, achievement_id)
VALUES (1, 2),
       (1, 1),
       (2, 2),
       (3, 1),
       (3, 3);

INSERT INTO habits(label, icon)
VALUES ('Faire du sport', 'sport'),
       ('Reviser', 'study'),
       ('Bien manger', 'food' );

INSERT INTO user_habits(user_id, habits_id, label, icon)
VALUES (1, NULL, 'Voir ses amis', 'user'),
       (1, 2, 'Etudier l\'espagnole', 'study'),
       (1, NULL, 'Bien manger' , 'user'),
       (2, NULL, 'Voir mes amis', 'static/user.png'),
       (2, 1, 'Se lever tôt', 'static/early.png'),
       (3, 2, 'Se coucher tôt', 'static/bed.png'),
       (3, 3, 'Bien manger', 'static/vegetables.png');

INSERT INTO habits_status(user_habits_id, for_date, `status`)
VALUES (1, CURRENT_DATE, 'done'),
       (1, '2022-09-11', 'undone'),
       (2, '2022-09-12', 'done'),
       (3, '2022-09-12', 'done'),
       (3, '2022-09-12', 'undone'),
       (4, '2022-09-12', 'done'),
       (5, '2022-09-11', 'undone'),
       (6, '2022-08-10', 'done');


SELECT * FROM user_habits
WHERE user_id = 1;

SELECT * FROM habits
WHERE id NOT IN
    (
        SELECT habits_id
        FROM user_habits
        WHERE user_id = 1
        AND habits_id IS NOT NULL
    );


SELECT habits_id
FROM user_habits
WHERE user_id = 1
AND habits_id IS NOT NULL ;






