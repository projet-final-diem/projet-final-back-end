package com.example.simplon.repository;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import com.example.simplon.entity.Habit;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;




@SpringBootTest
@Sql({"/DB.sql"})
class HabitRepositoryTest {

	@Autowired
	HabitRepository habitRepository;

	@Test
	void findAll() {
		List<Habit> habitList = habitRepository.findAll();
		assertThat(habitList.get(0))
				.isNotNull()
				.hasFieldOrProperty("label")
				.hasFieldOrProperty("icon");

		assertThat(habitList.get(1))
				.isNotNull()
				.hasFieldOrProperty("label")
				.hasFieldOrProperty("icon");
	}

	@Test
	void findById() {
		Habit habitTest = habitRepository.findById(1);
		assertThat(habitTest)
				.isNotNull()
				.hasFieldOrProperty("label")
				.hasFieldOrProperty("icon");
	}

	@Test
	void findByIdNotFound() {
		Habit habitTest = habitRepository.findById(99);
		assertThat(habitTest).isNull();
	}

	@Test
	void getHabitsMinusAlreadySubscribedByUser() {
		List<Habit> habits = habitRepository.getHabitsMinusAlreadySubscribedByUser(1);
		assertThat(habits.get(0))
				.hasFieldOrPropertyWithValue("id", 1)
				.hasFieldOrPropertyWithValue("label", "Se lever tôt")
				.hasFieldOrPropertyWithValue("icon", "static/early.png");
		assertThat(habits.get(1))
				.hasFieldOrPropertyWithValue("id", 3)
				.hasFieldOrPropertyWithValue("label", "Bien manger")
				.hasFieldOrPropertyWithValue("icon", "static/vegetables.png");
	}

	@Test
	void getHabitsMinusAlreadySubscribedByUserNotFound() {
		List<Habit> habits = habitRepository.getHabitsMinusAlreadySubscribedByUser(99);
		assertThat(habits.size()).isEqualTo(3);
	}
}