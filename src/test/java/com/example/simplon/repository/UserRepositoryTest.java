package com.example.simplon.repository;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;



import com.example.simplon.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest
@Sql({"/DB.sql"})
class UserRepositoryTest {

	@Autowired
	UserRepository userRepository;

	@Test
	void save() {
		User user = new User(
				"Test",
				"test@test.com",
				"1234"
		);
		userRepository.save(user);
		assertThat(user.getId()).isNotNull();
	}

	@Test
	void findUserById() {
		User user = userRepository.findUserById(1);
		assertThat(user)
				.hasNoNullFieldsOrProperties()
				.hasFieldOrProperty("username")
				.hasFieldOrProperty("email")
				.hasFieldOrProperty("password");
	}

	@Test
	void findUserByIdNotFound() {
		User user = userRepository.findUserById(99);
		assertThat(user).isNull();
	}


	@Test
	void findUserByEmail() {
		User user = userRepository.findUserByEmail("elodie@test.com");
		assertThat(user)
				.hasNoNullFieldsOrProperties()
				.hasFieldOrProperty("username")
				.hasFieldOrProperty("email")
				.hasFieldOrProperty("password");
	}

	@Test
	void findUserByEmailNotFound() {
		User user = userRepository.findUserByEmail("olaf@test.com");
		assertThat(user).isNull();
	}



}