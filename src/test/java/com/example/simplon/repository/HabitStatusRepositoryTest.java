package com.example.simplon.repository;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import com.example.simplon.entity.HabitStatus;
import com.example.simplon.entity.User;
import java.time.LocalDate;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest
@Sql({"/DB.sql"})
class HabitStatusRepositoryTest {

	@Autowired
	HabitStatusRepository habitStatusRepository;

	@Test
	void save() {
		HabitStatus habitStatus = new HabitStatus(
				LocalDate.parse("2022-06-12"),
				"done",
				1
		);
		habitStatusRepository.save(habitStatus);
		assertThat(habitStatus.getId()).isNotNull();
	}

	@Test
	void updateStatusById() {
		HabitStatus habitStatus = new HabitStatus(
				1,
				LocalDate.parse("2022-06-12"),
				"undone"
		);
		assertThat(habitStatusRepository.updateStatusById(habitStatus)).isTrue();
	}

	@Test
	void updateStatusByIdDoNotExists() {
		HabitStatus habitStatus = new HabitStatus(
				10,
				LocalDate.parse("2022-06-12"),
				"undone"
		);
		assertThat(habitStatusRepository.updateStatusById(habitStatus)).isFalse();
	}
}