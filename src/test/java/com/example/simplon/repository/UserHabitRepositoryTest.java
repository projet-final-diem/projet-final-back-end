package com.example.simplon.repository;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import com.example.simplon.entity.Habit;
import com.example.simplon.entity.UserHabit;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

@SpringBootTest
@Sql({"/DB.sql"})
class UserHabitRepositoryTest {

	@Autowired
	UserHabitRepository userHabitRepository;

	@Test
	void saveWithoutHabit() {
		UserHabit userHabit = new UserHabit(
				1,
				"test",
				"/test"
		);
		userHabitRepository.save(userHabit);
		assertThat(userHabit.getUserId()).isNotNull();
	}

	@Test
	void saveWithHabit() {
		UserHabit userHabit = new UserHabit(
				1,
				null,
				null
		);
		Habit habit = new Habit(
				1,
				"test",
				"/test"
		);
		userHabit.setHabit(habit);
		userHabitRepository.save(userHabit);
		assertThat(userHabit.getUserId()).isNotNull();
	}

	@Test
	void getAllUserHabitByUserId() {
		List<UserHabit> list = userHabitRepository.getAllUserHabitByUserId(1);
		assertThat(list.size()).isEqualTo(2);
		assertThat(list.get(1))
				.isNotNull()
				.hasFieldOrProperty("userId")
				.hasFieldOrProperty("habitId")
				.hasFieldOrProperty("label")
				.hasFieldOrProperty("icon");
	}

	@Test
	void getAllUserHabitByUserWhenUserHaveNone() {
		List<UserHabit> list = userHabitRepository.getAllUserHabitByUserId(4);
		assertThat(list.size()).isEqualTo(0);
	}

	@Test
	void deleteFound() {
		assertThat(userHabitRepository.delete(1)).isTrue();
	}

	@Test
	void deleteNotFound() {
		assertThat(userHabitRepository.delete(99)).isFalse();
	}

	@Test
	void getDailyHabitsFeedByUserId() {
		List<UserHabit> list = userHabitRepository.getDailyHabitsFeedByUserId(1);
		assertThat(list.size()).isEqualTo(2);
		assertThat(list.get(1))
				.isNotNull()
				.hasFieldOrProperty("userId")
				.hasFieldOrProperty("habitId")
				.hasFieldOrProperty("label")
				.hasFieldOrProperty("icon")
				.hasFieldOrProperty("dailyHabitStatus");
		assertThat(list.get(0).getDailyHabitStatus())
				.isNotNull()
				.hasFieldOrProperty("id")
				.hasFieldOrProperty("forDate")
				.hasFieldOrProperty("status")
				.hasFieldOrProperty("userHabitID");
	}

	@Test
	void getDailyHabitsFeedByUserIdThatNotExists() {
		List<UserHabit> list = userHabitRepository.getDailyHabitsFeedByUserId(99);
		assertThat(list.size()).isEqualTo(0);
	}

	@Test
	void getById() {
		UserHabit userHabit = userHabitRepository.getById(1);
		assertThat(userHabit)
				.isNotNull()
				.hasFieldOrProperty("userId")
				.hasFieldOrProperty("habitId")
				.hasFieldOrProperty("label")
				.hasFieldOrProperty("icon")
				.hasFieldOrProperty("dailyHabitStatus");
	}
	@Test
	void getByIdNotFound() {
		UserHabit userHabit = userHabitRepository.getById(99);
		assertThat(userHabit)
				.isNull();
	}
}