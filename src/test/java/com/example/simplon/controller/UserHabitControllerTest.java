package com.example.simplon.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.example.simplon.entity.Habit;
import com.example.simplon.entity.User;
import com.example.simplon.entity.UserHabit;
import com.example.simplon.frontEntity.UserFront;
import com.example.simplon.repository.UserHabitRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
@Sql({"/DB.sql"})
class UserHabitControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper mapper;

	@MockBean
	UserHabitRepository userHabitRepository;

	@Test
	void saveUserHabitWithHabit() throws Exception {
		UserHabit userHabit = new UserHabit(1, null, null);
		Habit habit = new Habit(1, "Se lever tôt", "static/early.png");
		userHabit.setHabit(habit);
		String json = mapper.writeValueAsString(userHabit);
		when(userHabitRepository.save(userHabit)).thenReturn(true);
		this.mockMvc.perform(post("/userhabit")
						.contentType(MediaType.APPLICATION_JSON)
						.content(json)
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	@Test
	void saveUserHabitWithoutHabit() throws Exception {
		UserHabit userHabit = new UserHabit(1, "test", "/test");
		String json = mapper.writeValueAsString(userHabit);
		when(userHabitRepository.save(userHabit)).thenReturn(true);
		this.mockMvc.perform(post("/userhabit")
						.contentType(MediaType.APPLICATION_JSON)
						.content(json)
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	@Test
	void getHabitByUserId() throws Exception {
		this.mockMvc.perform(get("/userhabit/1"))
				.andExpect(status().isOk()
				);
	}

	@Test
	void deleteFound() throws Exception {
		when(userHabitRepository.delete(1)).thenReturn((true));
		this.mockMvc.perform(delete("/userhabit/1"))
				.andExpect(status().isOk()
				);
	}

	@Test
	void deleteNotFound() throws Exception {
		this.mockMvc.perform(delete("/userhabit/99"))
				.andExpect(status().isNotFound()
				);
	}

	@Test
	void getDailyHabitsFeedByUserIdWithGoodId() throws Exception {
		this.mockMvc.perform(get("/userhabit/today/1"))
				.andExpect(status().isOk());
	}

	@Test
	void getDailyHabitsFeedByUserIdWithUnvalidId() throws Exception {
		this.mockMvc.perform(get("/userhabit/today/99"))
				.andExpect(status().isOk());
	}

	@Test
	void getUserHabitById() throws Exception {
		UserHabit userHabit = new UserHabit(1, "test", "/test");
		when(userHabitRepository.getById(1)).thenReturn(userHabit);
		this.mockMvc.perform(get("/userhabit/unique/1"))
				.andExpect(status().isOk());
		verify(userHabitRepository.getById(1));
	}

	@Test
	void getUserHabitByIdNotFound() throws Exception {
		UserHabit userHabit = new UserHabit(1, "test", "/test");
		when(userHabitRepository.getById(99)).thenReturn(null);
		this.mockMvc.perform(get("/userhabit/unique/99"))
				.andExpect(status().isNotFound());
		verify(userHabitRepository.getById(99));
	}
}
