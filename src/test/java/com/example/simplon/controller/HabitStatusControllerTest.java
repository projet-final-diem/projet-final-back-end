package com.example.simplon.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.example.simplon.entity.HabitStatus;
import com.example.simplon.repository.HabitStatusRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalDate;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
@Sql({"/DB.sql"})
class HabitStatusControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper mapper;

	@MockBean
	HabitStatusRepository habitStatusRepository;

	@Test
	void save() throws Exception{
		HabitStatus habitStatus = new HabitStatus(
				LocalDate.parse("2022-12-07"),
				"done",
				2
		);
		String json = mapper.writeValueAsString(habitStatus);
		this.mockMvc.perform(post("/habit-status")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	@Test
	void updateHabitStatus() throws Exception{
		HabitStatus habitStatusTest = new HabitStatus(
				1,
				LocalDate.parse("2022-06-12"),
				"undone"
		);
		String json = mapper.writeValueAsString(habitStatusTest);
		when(habitStatusRepository.updateStatusById(Mockito.any())).thenReturn(true);
		this.mockMvc.perform(put("/habit-status")
						.contentType(MediaType.APPLICATION_JSON)
						.content(json)
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	@Test
	void updateHabitStatusNotFound() throws Exception{
		HabitStatus habitStatus = new HabitStatus(
				20,
				LocalDate.parse("2022-12-07"),
				"done",
				1
		);
		String json = mapper.writeValueAsString(habitStatus);
		when(habitStatusRepository.updateStatusById(Mockito.any())).thenReturn(false);
		this.mockMvc.perform(put("/habit-status")
						.contentType(MediaType.APPLICATION_JSON)
						.content(json)
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}
}