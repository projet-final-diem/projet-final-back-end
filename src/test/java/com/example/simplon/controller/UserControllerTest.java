package com.example.simplon.controller;


import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.example.simplon.entity.User;
import com.example.simplon.frontEntity.UserFront;
import com.example.simplon.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.server.ResponseStatusException;


@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper mapper;

	@MockBean
	UserRepository userRepository;

	@Test
	void save() throws Exception {
		UserFront user = new UserFront("test", "test@test.com", "1234");
		String json = mapper.writeValueAsString(user);
		this.mockMvc.perform(post("/user")
						.contentType(MediaType.APPLICATION_JSON)
						.content(json)
						.accept(MediaType.APPLICATION_JSON))
						.andExpect(status().isOk());
	}
	@Test
	void saveAlreadyExists() throws Exception {
		UserFront user = new UserFront("test", "nicolas@test.com", "1234");
		User Mockuser = new User("test", "nicolas@test.com", "1234");
		String json = mapper.writeValueAsString(user);
		when(userRepository.findUserByEmail(user.getEmail())).thenReturn(Mockuser);
		this.mockMvc.perform(post("/user")
						.contentType(MediaType.APPLICATION_JSON)
						.content(json)
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}

	@Test
	void getById() throws Exception{
		User user = new User("test", "test@test.com", "1234");
		when(userRepository.findUserById(1)).thenReturn(user);
		this.mockMvc.perform(get("/user/1"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.userName").value("test"));
		}

	@Test
	void getByIdNoFound() throws Exception{
		User user = new User("test", "test@test.com", "1234");
		when(userRepository.findUserById(99)).thenReturn(null);
		this.mockMvc.perform(get("/user/99"))
				.andExpect(status().isNotFound());
	}

	@Test
	void userExists() throws Exception {
		when(userRepository.findUserByEmail("test@test.com")).thenReturn(new User());
		mockMvc.perform(get("/user/verify/test@test.com"))
				.andExpect(status().isNoContent());
	}

	@Test
	void userExistsNotFound() throws Exception {
		when(userRepository.findUserByEmail("test@test.com")).thenReturn(null);
		mockMvc.perform(get("/user/verify/test@test.com"))
				.andExpect(status().isNotFound());
	}

	@Test
	void login() throws Exception {
		// Pas trouvé comment tester ça...
	}

}