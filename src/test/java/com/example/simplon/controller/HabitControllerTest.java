package com.example.simplon.controller;


import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.example.simplon.entity.Habit;
import com.example.simplon.repository.HabitRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
@Sql({"/DB.sql"})
class HabitControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper mapper;

	@MockBean
	HabitRepository habitRepository;

	@Test
	void getAll() throws Exception {
		this.mockMvc.perform(get("/habit"))
				.andExpect(status().isOk());
	}

	@Test
	void getById() throws Exception {
		Habit habit = new Habit("test", "/test");
		when(habitRepository.findById(1)).thenReturn(habit);
		this.mockMvc.perform(get("/habit/1"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.label").value("test"));
	}

	@Test
	void getByIdNotFound() throws Exception {
		when(habitRepository.findById(99)).thenReturn(null);
		this.mockMvc.perform(get("/habit/99"))
				.andExpect(status().isNotFound());
	}

	@Test
	void getAllWithoutUserAlreadySubscribe() throws Exception {
		List<Habit> habits = List.of(
				new Habit(1, "test", "test"),
				new Habit(3, "test", "test")
		);
		when(habitRepository.getHabitsMinusAlreadySubscribedByUser(1)).thenReturn(habits);
		this.mockMvc.perform(get("/habit/user/1"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$").isArray())
				.andExpect(jsonPath("$[0].id").value(1))
				.andExpect(jsonPath("$[0].label").value("test"))
				.andExpect(jsonPath("$[1].id").value("3"))
				.andExpect(jsonPath("$[1].icon").value("test"));

		verify(habitRepository).getHabitsMinusAlreadySubscribedByUser(1);
	}

	@Test
	void getAllWithoutUserAlreadySubscribeWithEmptySet() throws Exception {
		List<Habit> habits = new ArrayList<>();
		when(habitRepository.getHabitsMinusAlreadySubscribedByUser(1)).thenReturn(habits);
		this.mockMvc.perform(get("/habit/user/1"))
				.andExpect(status().isOk());
	}

}